A collection of some of my personal scripts.

All scripts are POSIX compliant, meaning you should be able to execute them with most shells (bash, zsh, ...), while others are not guaranteed to work (fish).

Make sure you are able to execute them:

```bash
chmod +x (file name)
```

> Friendly advice: Please read the script you are going to use, do not just blindly execute it!

# List of scripts

The following is a description of what each script does, and what they output when being ran as `(command) --help`.

* `encrypt_dir`: 

```bash

    Usage: encrypt_dir <directory>

    Uses 7z to compress a directory with a password.

	-?, -h, --help
		Display this help message

```

* `generate_readme`: 

```bash

	Usage: generate_readme

    Generates the README.md of this repository.

	-?, -h, --help
		Display this help message

```

* `getredditimg`: 

```bash

	Usage: getredditimg [options] <subreddit>... [output directory]

	-?, -h, --help
		Display this help message

	-a, --all
		All images from the page that meet the criteria will be downloaded

	-H=HEIGHT, --min-height=HEIGHT
		Minimum height criteria to save the image. Images should be greater or equal to HEIGHT
		Default is 0

	-r=[RATIO][+E], --ratio=[RATIO][+E]
		Ratio that images should follow (width / height), within an error E
		Default is 1.77+0.5

	-t=TIME, --time=TIME
		TIME can be 'day', 'week', 'month', 'year', or 'all'
		Default is 'day'

	-q, --quiet
		Don't print to standard output

	-v, --verbose
		Print extra information to standard output

	-W=WIDTH, --min-width=WIDTH
		Minimum width criteria to save the image. Images should be greater or equal to WIDTH
		Default is 1920

    Some good subreddits with high-quality beautiful landscapes (they are NOT +18, despite the name!):
		earthporn	    seaporn 		    skyporn 	pic     	futureporn
		cityporn	    abandonedporn		historyporn	viewporn	architectureporn
		cozyplaces	    natureisfuckinglit	wallpapers	wallpaper	spaceporn
		oldschoolcool   imaginarylandscapes

		check more at: https://www.reddit.com/r/sfwpornnetwork/wiki/network

    For example, you can run the following:

        getredditimg -qvat=month wallpapers

```

* `getsize`: 

```bash

	Usage: getsize [options] <program | file | github url | hub.docker url>...

	-?, -h, --help
		Show this help message

	-p, --programs
		Show size of programs, not files
		If no program is given, show size for all programs in PATH
		If you use globbing characters ('*', '?') make sure you
		aren't accidentally choosing files in your current directory

	-s, --sort
		Sort sizes in ascending order

	-t, --total
		Only display the total

```

* `getusage`: 

```bash

    Usage: getusage <unit.slice | .service | .scope>...

    Provided a cgroupsv2 / systemd slice or service, continuously print its memory and
    CPU usage. Press q to exit.

    -?, -h, --help
        Display this help message.

    For example, you can show the app.slice usage (user applications slice) like,

        getusage app.slice

    You can also show all slices and services that start with 'user',

        getusage user

    Or, to see all services,

        getusage '*.service'

```

* `getytvideo`: 

```bash

    Usage: getytvideo [options] <search query>... [output directory]

    Query Youtube anonymously (Youtube still knows your IP) and
    optionally download the videos as audio files.

    -?, -h, --help
        Display this help message

    -a, --all
        Print all results in page

    -d, --download
        Use youtube-dl to download the found videos as audio files

    -f=FILE, --file=FILE
        Read queries from file

    -n=MAX, --number-search=MAX
        Maximum no. of re-tries for the search
        Default is 10

    -q, --quiet
        Do not print to standard output

    -u, --url
        Get video id in url format

    -v, --verbose
        Print extra information to standard output

    For example, you can search and download music genres like:

        getytvideo -vuad 'pop music' 'rap' 'classical music'

```

