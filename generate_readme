#!/bin/sh

if [ "$1" = "-h" ] || [ "$1" = "-?" ] || [ "$1" = "--help" ] ; then
	cat <<__EOT__

	Usage: $(basename "$0")

    Generates the README.md of this repository.

	-?, -h, --help
		Display this help message

__EOT__
	exit 0
fi

# exit if any command has an error
set -e

cat >README.md <<EOF
A collection of some of my personal scripts.

All scripts are POSIX compliant, meaning you should be able to execute them with most shells (bash, zsh, ...), while others are not guaranteed to work (fish).

Make sure you are able to execute them:

\`\`\`bash
chmod +x (file name)
\`\`\`

> Friendly advice: Please read the script you are going to use, do not just blindly execute it!

# List of scripts

The following is a description of what each script does, and what they output when being ran as \`(command) --help\`.

EOF

for file in $(git ls-files); do
    [ -x "$file" ] || continue
    echo "* \`$file\`: " >> README.md
    echo ""              >> README.md
    echo "\`\`\`bash"    >> README.md
    $file --help         >> README.md
    echo "\`\`\`"        >> README.md
    echo ""              >> README.md
done

echo "README.md generated successfully"

exit 0
